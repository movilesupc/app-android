package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults

class SplashActivity : AppCompatActivity() {
    private val SPLASH_DISPLAY_LENGTH = 2300
    private val UI_ANIMATION_DELAY = 10
    private val mHideHandler = Handler()
    private var mContentView: View? = null

    private val mHidePart2Runnable = Runnable {
        mContentView!!.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mContentView = findViewById(R.id.backgroundConstraintLayout)
        hide()

        if (UserDefaults.isLogged(this)) startMainActivity()
        else Handler().postDelayed({ startLoginActivity() }, SPLASH_DISPLAY_LENGTH.toLong())
    }

    private fun startLoginActivity() {
        val loginIntent = Intent(this@SplashActivity, LoginActivity::class.java)
        this@SplashActivity.startActivity(loginIntent)
        finish()
    }

    private fun startMainActivity() {
        UserDefaults.loadFromSharedPreferences(this)

        val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
        this@SplashActivity.startActivity(mainIntent)
        finish()
    }

    private fun hide() {
        val actionBar = supportActionBar
        actionBar?.hide()
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }
}
