package pe.com.teammoviles.yourwayapp.ui.models

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class SportEventUser(

    @SerializedName("id")
    var id: String,

    @SerializedName("eventId")
    var eventId: String,

    @SerializedName("userId")
    var userId: String,

    @SerializedName("user")
    var user: User
) : Serializable {

}