package pe.com.teammoviles.yourwayapp.ui.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_add_friend_event.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.adapters.AddFriendEventAdapter
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddFriendEventActivity : AppCompatActivity() {

    companion object {
        var friendsSelected: MutableList<User> = ArrayList()
    }

    private lateinit var friendAdapter: AddFriendEventAdapter
    private lateinit var friends: MutableList<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_friend_event)
        backAppCompatImageButton.setOnClickListener { finish() }
        friendsRecyclerView.layoutManager = LinearLayoutManager(this)
        acceptCardView.setOnClickListener { finish() }
        getFriends()
    }

    private fun getFriends() {
        showProgressBar()
        IEventApi.create().getFriends(UserDefaults.id!!).enqueue(object :
            Callback<MutableList<User>?> {
            override fun onFailure(call: Call<MutableList<User>?>, t: Throwable) {
                hideProgressBar()
                showMessageTextView("Error al obtener los amigos del usuario")
            }
            override fun onResponse(call: Call<MutableList<User>?>, response: Response<MutableList<User>?>) {
                hideProgressBar()
                hideMessageTextView()

                if (response.isSuccessful) {
                    friends = response.body()!!
                    checkAllPreviousSelected(friends)

                    friendAdapter = AddFriendEventAdapter(friends)
                    friendAdapter.setOnItemClicked(object : AddFriendEventAdapter.OnItemClicked {
                        override fun itemAddClicked(friend: User) {
                            addFriendToParticipants(friend)
                        }
                        override fun itemDeleteClicked(friend: User) {
                            deleteFriendFromParticipants(friend)
                        }
                    })

                    friendsRecyclerView.adapter = friendAdapter
                    friendAdapter.notifyDataSetChanged()
                    if (friends.count() == 0)
                        showMessageTextView("No tiene amigos agregados")
                }
                else {
                    showMessageTextView("Error al obtener los amigos del usuario")
                }
            }
        })
    }

    fun addFriendToParticipants(friend: User) {
        friendsSelected.add(friend)
    }

    fun deleteFriendFromParticipants(friend: User) {
        for (f in friendsSelected) {
            if (f.id == friend.id){
                friendsSelected.remove(f)
                break
            }
        }
    }

    fun checkAllPreviousSelected(friendsRecieved: MutableList<User>) {
        for (f in friendsRecieved) {
            checkAllPreviousSelected(f)
        }
    }

    fun checkAllPreviousSelected(friendRecieved: User) {
        for (f in friendsSelected) {
            if (f.id == friendRecieved.id){
                friendRecieved.isSelected = true
            }
        }
    }

    fun showProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.VISIBLE
        if (tittleDividerView != null)
            tittleDividerView.visibility = View.GONE
    }

    fun hideProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.GONE

        if (tittleDividerView != null)
            tittleDividerView.visibility = View.VISIBLE
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessageTextView(message: String) {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.VISIBLE

        if (messageTextView != null)
            messageTextView.text = message
    }

    fun hideMessageTextView() {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.GONE
    }
}
