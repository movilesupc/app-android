package pe.com.teammoviles.yourwayapp.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.prototype_place.view.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.*

class PlacesAdapter(val places: MutableList<Place>) : RecyclerView.Adapter<PlacesAdapterPrototype>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesAdapterPrototype {
        val placesPrototype: PlacesAdapterPrototype = PlacesAdapterPrototype(
            LayoutInflater.from(parent.context).inflate(
                R.layout.prototype_place, parent, false))
        return placesPrototype
    }

    override fun getItemCount(): Int {
        return places.size
    }

    override fun onBindViewHolder(holder: PlacesAdapterPrototype, position: Int) {
        val place = places[position]

        holder.bind(place)
        holder.placeCardView.setOnClickListener {
            onItemClicked?.apply {
                deseleccionarTodos()
                place.isSelected = true
                holder.placeCardView.setCardBackgroundColor(Color.parseColor("#757575"))
                itemClicked(place)
                notifyDataSetChanged()
            }
        }

        if (place.isSelected){
            holder.placeCardView.setCardBackgroundColor(Color.parseColor("#757575"))
        }
        else {
            holder.placeCardView.setCardBackgroundColor(Color.parseColor("#6FBF73"))
        }
    }

    private fun deseleccionarTodos() {
        for (place in places) {
            place.isSelected = false
        }
    }

    interface OnItemClicked {
        fun itemClicked(place: Place)
    }

    private var onItemClicked: OnItemClicked? = null

    fun setOnItemClicked(onItemClicked: OnItemClicked){
        this.onItemClicked = onItemClicked
    }
}

class PlacesAdapterPrototype(itemView: View): RecyclerView.ViewHolder(itemView) {
    private var nameTextView = itemView.placeNameTextView
    var placeCardView = itemView.placeCardView

    fun bind(place: Place) {
        nameTextView.text = place.name
    }
}