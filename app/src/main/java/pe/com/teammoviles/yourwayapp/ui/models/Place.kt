package pe.com.teammoviles.yourwayapp.ui.models

import androidx.room.Entity
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Place(

    @SerializedName("id")
    var id: String,

    @SerializedName("name")
    var name: String,

    @SerializedName("lat")
    var lat: String,

    @SerializedName("lng")
    var lng: String,

    @SerializedName("active")
    var active: String,

    @SerializedName("sportTypeId")
    var sportTypeId: String,

    var isSelected: Boolean = false
) : Serializable {
    constructor() : this("", "", "", "", "", "")

    val latLng: LatLng
        get() = LatLng(lat.toDouble(), lng.toDouble())
}
