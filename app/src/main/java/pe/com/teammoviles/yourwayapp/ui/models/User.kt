package pe.com.teammoviles.yourwayapp.ui.models

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class User(

    @SerializedName("id")
    var id: String?,

    @SerializedName("name")
    var name: String?,

    @SerializedName("lastName")
    var lastName: String?,

    @SerializedName("email")
    var email: String?,

    @SerializedName("imageUrl")
    var imageUrl: String?,

    var isSelected: Boolean = false
) : Serializable {

}