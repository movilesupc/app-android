package pe.com.teammoviles.yourwayapp.ui.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_add_friend.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.adapters.AddFriendAdapter
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.models.UserFriend
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddFriendActivity : AppCompatActivity() {

    private lateinit var friendAdapter: AddFriendAdapter
    private lateinit var friends: MutableList<User>
    private lateinit var allUsers: MutableList<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_friend)
        backAppCompatImageButton.setOnClickListener { finish() }
        friendsRecyclerView.layoutManager = LinearLayoutManager(this)
        getFriends()
    }

    private fun getFriends() {
        showProgressBar()
        IEventApi.create().getFriends(UserDefaults.id!!).enqueue(object :
            Callback<MutableList<User>?> {
            override fun onFailure(call: Call<MutableList<User>?>, t: Throwable) {
                hideProgressBar()
                showMessageTextView("Error al obtener los amigos del usuario")
            }
            override fun onResponse(call: Call<MutableList<User>?>, response: Response<MutableList<User>?>) {
                hideProgressBar()
                hideMessageTextView()

                if (response.isSuccessful) {
                    friends = response.body()!!
                }
                getAllUsers()
            }
        })
    }

    private fun getAllUsers() {
        showProgressBar()
        IEventApi.create().getUsers().enqueue(object :
            Callback<MutableList<User>?> {
            override fun onFailure(call: Call<MutableList<User>?>, t: Throwable) {
                hideProgressBar()
                showMessageTextView("Error al obtener los usuarios")
            }
            override fun onResponse(call: Call<MutableList<User>?>, response: Response<MutableList<User>?>) {
                hideProgressBar()
                hideMessageTextView()

                if (response.isSuccessful) {
                    allUsers = response.body()!!
                    checkAllUsers(allUsers)
                    friendAdapter = AddFriendAdapter(allUsers)
                    friendAdapter.setOnItemClicked(object : AddFriendAdapter.OnItemClicked {
                        override fun itemAddClicked(friend: User) {
                            addFriend(friend)
                        }
                        override fun itemDeleteClicked(friend: User) {
                            deleteFriend(friend)
                        }
                    })

                    friendsRecyclerView.adapter = friendAdapter
                    friendAdapter.notifyDataSetChanged()
                    if (allUsers.count() == 0)
                        showMessageTextView("No hay usuarios")
                }
                else {
                    showMessageTextView("Error al obtener los usuarios")
                }
            }
        })
    }

    fun checkAllUsers(usersRecieved: MutableList<User>) {
        for (user in usersRecieved) {
            checkAllUsers(user)
        }

        //elimino a mi mismo
        for (user in usersRecieved) {
            if (user.id == UserDefaults.id) {
                usersRecieved.remove(user)
                break
            }
        }
    }

    fun checkAllUsers(userRecieved: User) {
        for (f in friends) {
            if (f.id == userRecieved.id){
                userRecieved.isSelected = true
            }
        }
    }

    fun addFriend(friend: User) {
        showProgressBar()
        val json = JSONObject()
        json.put("userId", UserDefaults.id)
        json.put("friendId", friend.id)
        json.put("active", true)
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), json.toString())

        IEventApi.create().addFriend(requestBody).enqueue(object : Callback<UserFriend?> {
            override fun onFailure(call: Call<UserFriend?>, t: Throwable) {
                showToast("Error al agregar amigo")
                hideProgressBar()
            }

            override fun onResponse(call: Call<UserFriend?>, response: Response<UserFriend?>) {
                if (response.isSuccessful) {
                    friend.isSelected = true
                    friendAdapter.notifyDataSetChanged()
                }
                else
                    showToast("Error al agregar amigo")
                hideProgressBar()
            }
        })
    }

    fun deleteFriend(friend: User) {
        showProgressBar()
        IEventApi.create().deleteFriend(UserDefaults.id!!, friend.id!!).enqueue(object : Callback<UserFriend?> {
            override fun onFailure(call: Call<UserFriend?>, t: Throwable) {
                showToast("Error al eliminar el amigo")
                hideProgressBar()
            }

            override fun onResponse(call: Call<UserFriend?>, response: Response<UserFriend?>) {
                if (response.isSuccessful) {
                    friend.isSelected = false
                    friendAdapter.notifyDataSetChanged()
                }
                else
                    showToast("Error al eliminar el amigo")
                hideProgressBar()
            }
        })
    }

    fun showProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.VISIBLE
        if (tittleDividerView != null)
            tittleDividerView.visibility = View.GONE
    }

    fun hideProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.GONE

        if (tittleDividerView != null)
            tittleDividerView.visibility = View.VISIBLE
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessageTextView(message: String) {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.VISIBLE

        if (messageTextView != null)
            messageTextView.text = message
    }

    fun hideMessageTextView() {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.GONE
    }
}
