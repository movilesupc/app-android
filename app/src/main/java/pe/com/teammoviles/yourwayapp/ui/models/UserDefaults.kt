package pe.com.teammoviles.yourwayapp.ui.models

import android.content.Context

object UserDefaults {

    var id: String? = ""
    var name: String? = ""
    var lastName: String? = ""
    var email: String? = ""
    var imageUrl: String? = ""

    fun isLogged(context: Context): Boolean {
        val sh1 = context.getSharedPreferences("YourWayApp", Context.MODE_PRIVATE)
        id = sh1.getString("id", "")
        return id != ""
    }

    fun toUser(): User {
        return User( id, name, lastName, email, imageUrl, false)
    }

    fun saveInSharedPreferences(user: User, context: Context) {
        id = user.id
        name = user.name
        lastName = user.lastName
        email = user.email
        imageUrl = user.imageUrl
        saveInSharedPreferences(context)
    }

    private fun saveInSharedPreferences(context: Context) {
        val sh = context.getSharedPreferences("YourWayApp", Context.MODE_PRIVATE)
        val myEdit = sh.edit()
        myEdit.putString("id", id)
        myEdit.putString("name", name)
        myEdit.putString("lastName", lastName)
        myEdit.putString("email", email)
        myEdit.putString("imageUrl", imageUrl)
        myEdit.apply()
    }

    fun loadFromSharedPreferences(context: Context) {
        val sh1 = context.getSharedPreferences("YourWayApp", Context.MODE_PRIVATE)
        id = sh1.getString("id", "")
        name = sh1.getString("name", "")
        lastName = sh1.getString("lastName", "")
        email = sh1.getString("email", "")
        imageUrl = sh1.getString("imageUrl", "")
    }

    fun clearFromSharedPreferences(context: Context) {
        val sh = context.getSharedPreferences("YourWayApp", Context.MODE_PRIVATE)
        val myEdit = sh.edit()
        myEdit.putString("id", "")
        myEdit.putString("name", "")
        myEdit.putString("lastName", "")
        myEdit.putString("email", "")
        myEdit.putString("imageUrl", "")
        myEdit.apply()
    }
}