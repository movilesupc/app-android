package pe.com.teammoviles.yourwayapp.ui.retrofit

import okhttp3.RequestBody
import pe.com.teammoviles.yourwayapp.ui.models.Place
import pe.com.teammoviles.yourwayapp.ui.models.SportEvent
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserFriend
import retrofit2.Call
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface IEventApi {

    @POST("api/login")
    fun login(@Body request: RequestBody): Call<User>

    @POST("api/users")
    fun register(@Body request: RequestBody): Call<User>

    @GET("api/users")
    fun getUsers(): Call<MutableList<User>>

    @POST("api/users/1/friends")
    fun addFriend(@Body request: RequestBody): Call<UserFriend>

    @DELETE("api/users/{userId}/friends/{friendId}")
    fun deleteFriend(@Path(value = "userId") userId: String, @Path(value = "friendId") friendId: String): Call<UserFriend>

    @GET("api/sportEvents/user/{userId}")
    fun getEvents(@Path(value = "userId") userId: String): Call<MutableList<SportEvent>>

    @GET("api/sportEvents/asmember/{userId}")
    fun getEventsAsMember(@Path(value = "userId") userId: String): Call<MutableList<SportEvent>>

    @GET("api/users/{userId}/friends")
    fun getFriends(@Path(value = "userId") userId: String): Call<MutableList<User>>

    @GET("api/places")
    fun getPlaces(): Call<MutableList<Place>>

    @GET("api/sportEvents/{eventId}")
    fun getSportEventData(@Path(value = "eventId") eventId: String): Call<SportEvent>

    @POST("api/sportEvents")
    fun registerSportEvent(@Body request: RequestBody): Call<SportEvent>

    @PUT("api/sportEvents/{id}/rate/{rating}/")
    fun rateSportEvent(@Path(value = "id") id: String, @Path(value = "rating") rating: Float): Call<SportEvent>

    companion object {
        private const val BASE_URL = "https://yourwayapi.azurewebsites.net/"

        fun create(): IEventApi {
            val retrofit = retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(IEventApi::class.java)
        }
    }
}