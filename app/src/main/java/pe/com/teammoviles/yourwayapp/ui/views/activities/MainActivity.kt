package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.views.fragments.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val FRAGMENT_MY_EVENTS = 1
        const val FRAGMENT_MY_INVITED_EVENTS = 2
        const val FRAGMENT_MY_FRIENDS = 3
        const val FRAGMENT_MAPS = 4
        const val FRAGMENT_PROFILE = 5
        var CURRENT_FRAGMENT_SELECTED = 1
    }

    private val manager = supportFragmentManager
    //private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //auth = FirebaseAuth.getInstance()
        chooseFragment(bnMain.menu.findItem(R.id.itMyEvents))
        bnMain.setOnNavigationItemSelectedListener { item -> chooseFragment(item) }

        addFloatingActionButton.setOnClickListener {
            addAction()
        }
    }

    fun addAction() {
        if (CURRENT_FRAGMENT_SELECTED == FRAGMENT_MY_EVENTS){
            val intent = Intent(this, CreateSportEventActivity::class.java)
            startActivity(intent)
        }
        if (CURRENT_FRAGMENT_SELECTED == FRAGMENT_MY_FRIENDS){
            val intent = Intent(this, AddFriendActivity::class.java)
            startActivity(intent)
        }
    }

    private fun chooseFragment(item: MenuItem): Boolean {
        addFloatingActionButton.hide()

        when (item.itemId) {
            R.id.itMyEvents -> {
                addFloatingActionButton.show()
                addFragment(MyEventsFragment())
                CURRENT_FRAGMENT_SELECTED = FRAGMENT_MY_EVENTS
            }
            R.id.itMyInvitedEvents -> {
                addFragment(InvitedEventsFragment())
                CURRENT_FRAGMENT_SELECTED = FRAGMENT_MY_INVITED_EVENTS
            }
            R.id.itMyFriends -> {
                addFloatingActionButton.show()
                addFragment(FriendFragment())
                CURRENT_FRAGMENT_SELECTED = FRAGMENT_MY_FRIENDS
            }
            R.id.itMap -> {
                addFragment(MapFragment())
                CURRENT_FRAGMENT_SELECTED = FRAGMENT_MAPS
            }
            R.id.itProfile -> {
                addFragment(ProfileFragment())
                CURRENT_FRAGMENT_SELECTED = FRAGMENT_PROFILE
            }
        }
        return true
    }

    private fun addFragment(fragment: Fragment) {
        manager
            .beginTransaction()
            .replace(R.id.flMain, fragment)
            .commit()
    }
}
