package pe.com.teammoviles.yourwayapp.ui.views.fragments


import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_friend.*

import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.adapters.FriendAdapter
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class FriendFragment : Fragment() {
    private lateinit var friendAdapter: FriendAdapter
    private lateinit var friends: MutableList<User>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_friend, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvFriend.layoutManager = LinearLayoutManager(context)
    }

    override fun onResume() {
        getFriends()
        super.onResume()
    }

    private fun getFriends() {
        showProgressBar()
        IEventApi.create().getFriends(UserDefaults.id!!).enqueue(object :
            Callback<MutableList<User>?> {
            override fun onFailure(call: Call<MutableList<User>?>, t: Throwable) {
                hideProgressBar()
                showMessageTextView("Error al obtener los amigos del usuario")
            }

            override fun onResponse(call: Call<MutableList<User>?>, response: Response<MutableList<User>?>) {
                hideProgressBar()
                hideMessageTextView()
                if (response.isSuccessful) {
                    friends = response.body()!!
                    friendAdapter = FriendAdapter(friends)
                    rvFriend.adapter = friendAdapter
                    friendAdapter.notifyDataSetChanged()
                    if (friends.count() == 0)
                        showMessageTextView("No tiene amigos agregados")
                }
                else {
                    showMessageTextView("Error al obtener los amigos del usuario")
                }
            }
        })
    }

    fun showProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.VISIBLE
        if (tittleDividerView != null)
            tittleDividerView.visibility = View.GONE
    }

    fun hideProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.GONE

        if (tittleDividerView != null)
            tittleDividerView.visibility = View.VISIBLE
    }

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessageTextView(message: String) {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.VISIBLE

        if (messageTextView != null)
            messageTextView.text = message
    }

    fun hideMessageTextView() {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.GONE
    }
}