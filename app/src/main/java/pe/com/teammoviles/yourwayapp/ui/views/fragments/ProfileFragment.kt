package pe.com.teammoviles.yourwayapp.ui.views.fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_profile.*

import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.views.activities.LoginActivity


class ProfileFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title.text = UserDefaults.name + " " + UserDefaults.lastName

        tiName.text!!.clear()
        tiName.text!!.append(UserDefaults.name)
        tiLastName.text!!.clear()
        tiLastName.text!!.append(UserDefaults.lastName)
        tiLoginMail.text!!.clear()
        tiLoginMail.text!!.append(UserDefaults.email)
        tiPassword.text!!.clear()
        tiPassword.text!!.append("******")

        cerrarSesionCardView.setOnClickListener {
            showDialogSimple("Cerrar sesióm", "Seguro que desea cerrar sesión")
        }
    }

    fun showDialogSimple(titulo: String?, detalle: String?) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(titulo)
        builder.setMessage(detalle)
        builder.setPositiveButton("Ok"){dialog, which ->
            UserDefaults.clearFromSharedPreferences(context!!)
            startLoginActivity()
        }
        builder.setNegativeButton("Cancel"){dialog, which ->}
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun startLoginActivity() {
        val loginIntent = Intent(context, LoginActivity::class.java)
        this.startActivity(loginIntent)
        activity!!.finish()
    }
}










































