package pe.com.teammoviles.yourwayapp.ui.views.fragments

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.checkSelfPermission

import pe.com.teammoviles.yourwayapp.R
import java.util.ArrayList
import android.util.DisplayMetrics
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import android.location.LocationManager
import android.content.Context.LOCATION_SERVICE
import android.content.Context
import android.widget.Toast
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import pe.com.teammoviles.yourwayapp.ui.models.Place
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    internal var mapView: MapView? = null
    internal var mMap: GoogleMap? = null
    internal lateinit var mView: View

    private var permissionsToRequest: ArrayList<String>? = null
    private val permissionsRejected = ArrayList<String>()
    private val permissions = ArrayList<String>()

    private val ALL_PERMISSIONS_RESULT = 101

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    internal var markers: MutableList<Marker> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_map, container, false)
        initPermissions()
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView = mView.findViewById(R.id.map)
        if (mapView != null) {
            mapView!!.onCreate(null)
            mapView!!.onResume()
            mapView!!.getMapAsync(this)
        }
    }

    private fun initPermissions() {
        permissions.add(ACCESS_FINE_LOCATION)
        permissions.add(ACCESS_COARSE_LOCATION)
        permissionsToRequest = findUnAskedPermissions(permissions)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest!!.size > 0)
                requestPermissions(permissionsToRequest!!.toTypedArray(), ALL_PERMISSIONS_RESULT)
        }
    }

    private fun findUnAskedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()

        for (perm in wanted) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }

        return result
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return checkSelfPermission(context!!, permission) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        MapsInitializer.initialize(context)

        mMap = googleMap
        if (mMap != null) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
            mMap!!.uiSettings.isZoomControlsEnabled = false
            mMap!!.setOnMarkerClickListener(this)
            mMap!!.setPadding(dpToPx(20),0,0,dpToPx(20))

            mMap!!.isMyLocationEnabled = true
            fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15f))
                }
            }

            if (!isGPSEnabled(context!!)){
                showToast("Active su gps")
            }

            //una vez cargado el mapa obtengo los lugares deportivos
            getPlaces()
        }
    }

    private fun getPlaces() {
        IEventApi.create().getPlaces().enqueue(object : Callback<MutableList<Place>?> {
            override fun onFailure(call: Call<MutableList<Place>?>, t: Throwable) {
                showToast("Error al obtener los lugares cercanos")
            }
            override fun onResponse(call: Call<MutableList<Place>?>, response: Response<MutableList<Place>?>) {
                if (response.isSuccessful) {
                    setMarkers(response.body()!!)
                }
                else {
                    showToast("Error al obtener los lugares cercanos")
                }
            }
        })
    }

    private fun setMarkers(places: MutableList<Place>) {
        mMap!!.clear()
        markers.clear()

        for (place in places) {
            val marker = mMap!!.addMarker(
                MarkerOptions()
                    .position(place.latLng)
                    .title(Gson().toJson(place))
                    .snippet("")
            )
            markers.add(marker)
        }
    }

    private fun isGPSEnabled(mContext: Context): Boolean {
        val lm = mContext.getSystemService(LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        return true
    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = context!!.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    private fun showToast(message: String) {
        Toast.makeText(context!!, message, Toast.LENGTH_SHORT).show()
    }
}
