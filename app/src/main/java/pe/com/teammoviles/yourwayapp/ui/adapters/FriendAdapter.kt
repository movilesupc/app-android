package pe.com.teammoviles.yourwayapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.prototype_friend.view.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.*

class FriendAdapter(val friends: MutableList<User>) : RecyclerView.Adapter<FriendPrototype>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendPrototype {
        val myPrototype: FriendPrototype = FriendPrototype(
            LayoutInflater.from(parent.context).inflate(
                R.layout.prototype_friend, parent, false))
        return myPrototype
    }

    override fun getItemCount(): Int {
        return friends.size
    }

    override fun onBindViewHolder(holder: FriendPrototype, position: Int) {
        holder.bind(friends[position])
    }
}

class FriendPrototype(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener{
    private var tvFriendName = itemView.tvFriendName
    private var tvFriendLastName = itemView.tvFriendLastName
    private var tvFriendEmail = itemView.tvFriendEmail
    private var ivFriendImg = itemView.ivFriendImg
    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

    }

    fun bind(user: User) {
        tvFriendName.text = user.name
        tvFriendLastName.text = user.lastName
        tvFriendEmail.text = user.email
    }
}