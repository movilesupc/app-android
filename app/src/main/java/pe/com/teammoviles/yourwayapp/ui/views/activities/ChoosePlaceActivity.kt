package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_choose_place.*
import kotlinx.android.synthetic.main.activity_choose_place.loadProgressBar
import kotlinx.android.synthetic.main.activity_choose_place.tittleDividerView
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.adapters.PlacesAdapter
import pe.com.teammoviles.yourwayapp.ui.models.Place
import pe.com.teammoviles.yourwayapp.ui.models.SportEvent
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChoosePlaceActivity : AppCompatActivity(), OnMapReadyCallback {
    internal var mapView: MapView? = null
    internal var mMap: GoogleMap? = null
    internal var marker: Marker? = null

    private var permissionsToRequest: ArrayList<String>? = null
    private val permissionsRejected = ArrayList<String>()
    private val permissions = ArrayList<String>()

    private val ALL_PERMISSIONS_RESULT = 101

    private lateinit var placesAdapter: PlacesAdapter
    private var placeSelected: Place? = null
    private lateinit var places: MutableList<Place>

    private lateinit var sportEventCrear: SportEvent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_place)
        initPermissions()

        backAppCompatImageButton.setOnClickListener { finish() }
        nextCardView.setOnClickListener {
            if (placeSelected != null)
                createEvent()
            else
                showToast("Seleccione un lugar")
        }
        placesRecyclerView.layoutManager = LinearLayoutManager(this)
        getPlaces()

        //1 = privada, 2 = publico
        privacidadMaterialSpinner.setItems(
            "Privada",
            "Publica"
        )
        privacidadMaterialSpinner.selectedIndex = 1

        mapView = findViewById(R.id.map)
        if (mapView != null) {
            mapView!!.onCreate(null)
            mapView!!.onResume()
            mapView!!.getMapAsync(this)
        }

        intent?.extras?.apply {
            sportEventCrear = getSerializable("sportEventCrear") as SportEvent
        }
    }

    private fun createEvent() {
        nextCardView.visibility = View.GONE
        showProgressBar()

        sportEventCrear.placeId = placeSelected!!.id
        sportEventCrear.privacityTypeId = (privacidadMaterialSpinner.selectedIndex + 1).toString()

        val seJsonBody = JSONObject()
        seJsonBody.put("title", sportEventCrear.title)
        seJsonBody.put("description", sportEventCrear.description)
        seJsonBody.put("userId", sportEventCrear.userId)
        seJsonBody.put("sportTypeId", sportEventCrear.sportTypeId)
        seJsonBody.put("privacityTypeId", sportEventCrear.privacityTypeId)
        seJsonBody.put("placeId", sportEventCrear.placeId)
        seJsonBody.put("active", sportEventCrear.active)
        seJsonBody.put("date", sportEventCrear.date)

        val sebusJSONArray = JSONArray()
        for (sebu in AddFriendEventActivity.friendsSelected){
            val sebuJSONObject = JSONObject()
            sebuJSONObject.put("eventId", "0")
            sebuJSONObject.put("userId", sebu.id)

            sebusJSONArray.put(sebuJSONObject)
        }
        seJsonBody.put("sportEventByUsers", sebusJSONArray)

        val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), seJsonBody.toString())
        IEventApi.create().registerSportEvent(requestBody).enqueue(object : Callback<SportEvent?> {
            override fun onFailure(call: Call<SportEvent?>, t: Throwable) {
                showToast("Error al crear el evento")
            }
            override fun onResponse(call: Call<SportEvent?>, response: Response<SportEvent?>) {
                if (response.isSuccessful) {
                    AddFriendEventActivity.friendsSelected.clear()
                    showDialogSimple("Mensaje", "Se creó el evento correctamente")
                }
                else {
                    showMessageTextView("Error al crear el evento")
                }
            }
        })
    }

    fun showDialogSimple(titulo: String?, detalle: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(titulo)
        builder.setMessage(detalle)
        builder.setPositiveButton("Ok"){ dialog, which ->
            volverMain()
        }
        builder.setCancelable(false)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun volverMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        MapsInitializer.initialize(this@ChoosePlaceActivity)

        mMap = googleMap
        if (mMap != null) {
            addMarker(LatLng(-12.104897,-76.9642295))
        }
    }

    private fun addMarker(latLng: LatLng) {
        mMap!!.clear()
        if (marker != null)
            marker!!.remove()

        marker = mMap!!.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("")
                .snippet("")
        )
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
    }

    private fun initPermissions() {
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        permissionsToRequest = findUnAskedPermissions(permissions)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest!!.size > 0)
                requestPermissions(permissionsToRequest!!.toTypedArray(), ALL_PERMISSIONS_RESULT)
        }
    }

    private fun findUnAskedPermissions(wanted: java.util.ArrayList<String>): java.util.ArrayList<String> {
        val result = java.util.ArrayList<String>()

        for (perm in wanted) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }

        return result
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    private fun getPlaces() {
        showProgressBar()
        IEventApi.create().getPlaces().enqueue(object : Callback<MutableList<Place>?> {
            override fun onFailure(call: Call<MutableList<Place>?>, t: Throwable) {
                hideProgressBar()
                showMessageTextView("Error al obtener los lugares cercanos")
            }
            override fun onResponse(call: Call<MutableList<Place>?>, response: Response<MutableList<Place>?>) {
                hideProgressBar()
                hideMessageTextView()

                if (response.isSuccessful) {
                    places = response.body()!!
                    placesAdapter = PlacesAdapter(places)
                    placesAdapter.setOnItemClicked(object : PlacesAdapter.OnItemClicked {
                        override fun itemClicked(place: Place) {
                            placeSelected = place
                            addMarker(place.latLng)
                        }
                    })

                    placesRecyclerView.adapter = placesAdapter
                    placesAdapter.notifyDataSetChanged()
                    if (places.count() == 0)
                        showMessageTextView("No hay agregados")
                }
                else {
                    showMessageTextView("Error al obtener los lugares cercanos")
                }
            }
        })
    }

    fun showProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.VISIBLE
        if (tittleDividerView != null)
            tittleDividerView.visibility = View.GONE
    }

    fun hideProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.GONE

        if (tittleDividerView != null)
            tittleDividerView.visibility = View.VISIBLE
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessageTextView(message: String) {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.VISIBLE

        if (messageTextView != null)
            messageTextView.text = message
    }

    fun hideMessageTextView() {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.GONE
    }
}
