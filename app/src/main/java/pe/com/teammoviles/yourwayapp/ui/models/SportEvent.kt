package pe.com.teammoviles.yourwayapp.ui.models

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import pe.com.teammoviles.yourwayapp.R
import java.io.Serializable

@Entity
data class SportEvent(

    @SerializedName("id")
    var id: String,

    @SerializedName("title")
    var title: String,

    @SerializedName("description")
    var description: String,

    @SerializedName("userId")
    var userId: String,

    @SerializedName("sportTypeId")
    var sportTypeId: String,

    @SerializedName("privacityTypeId")
    var privacityTypeId: String,

    @SerializedName("rating")
    var rating: String,

    @SerializedName("placeId")
    var placeId: String,

    @SerializedName("active")
    var active: String,

    @SerializedName("date")
    var date: String,

    @SerializedName("place")
    var place: Place,

    @SerializedName("sportEventByUsers")
    var sportEventByUsers: MutableList<SportEventUser>
) : Serializable {
    val imageResource: Int
        get() = if(sportTypeId == "1") R.drawable.ic_footbal else if (sportTypeId == "2") R.drawable.ic_athlete else R.drawable.ic_basket

    val users: MutableList<User>
        get() = getUsersb()

    private fun getUsersb(): MutableList<User> {
        var users = ArrayList<User>()
        for (sportEvent in sportEventByUsers) {
            users.add(sportEvent.user)
        }

        return users
    }
}
