package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_event.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.adapters.FriendAdapter
import pe.com.teammoviles.yourwayapp.ui.adapters.FriendInEventAdapter
import pe.com.teammoviles.yourwayapp.ui.models.SportEvent
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable
import java.util.ArrayList

class DetailEventActivity : AppCompatActivity(), OnMapReadyCallback {
    internal var mapView: MapView? = null
    internal var mMap: GoogleMap? = null
    internal var marker: Marker? = null

    private var permissionsToRequest: ArrayList<String>? = null
    private val permissionsRejected = ArrayList<String>()
    private val permissions = ArrayList<String>()

    private val ALL_PERMISSIONS_RESULT = 101

    private lateinit var friendInEventAdapter: FriendInEventAdapter
    private lateinit var currentEvent: SportEvent
    private lateinit var friends: MutableList<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_event)
        initPermissions()

        intent?.extras?.apply {
            currentEvent = getSerializable("currentEvent") as SportEvent
            titleTextView.text = currentEvent.title
            subTitle.text = currentEvent.description
            sportTypeAppCompatImageView.setImageResource(currentEvent.imageResource)

            friends = currentEvent.users
            friendInEventAdapter = FriendInEventAdapter(friends)
            sportPlaceValueTextView.text = currentEvent.place.name
            participantsRecyclerView.layoutManager = LinearLayoutManager(
                this@DetailEventActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            participantsRecyclerView.adapter = friendInEventAdapter
            mapView = findViewById(R.id.map)
            if (mapView != null) {
                mapView!!.onCreate(null)
                mapView!!.onResume()
                mapView!!.getMapAsync(this@DetailEventActivity)
            }
        }

        backAppCompatImageButton.setOnClickListener { finish() }
        rateCardView.setOnClickListener {
            val intent = Intent(this@DetailEventActivity, RateSportEventActivity::class.java)
            intent.putExtra("currentEvent", currentEvent as Serializable)
            startActivityForResult(intent, 123)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (data != null){
                //if (data.extras != null){
                //    currentEvent.rating = data.getStringExtra("rate")!!
                //}
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        MapsInitializer.initialize(this@DetailEventActivity)

        mMap = googleMap
        if (mMap != null) {
            marker = mMap!!.addMarker(
                MarkerOptions()
                    .position(currentEvent.place.latLng)
                    .title(Gson().toJson(currentEvent.place))
                    .snippet("")
            )
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(currentEvent.place.latLng, 15f))
        }
    }

    private fun initPermissions() {
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        permissionsToRequest = findUnAskedPermissions(permissions)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest!!.size > 0)
                requestPermissions(permissionsToRequest!!.toTypedArray(), ALL_PERMISSIONS_RESULT)
        }
    }

    private fun findUnAskedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()

        for (perm in wanted) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }

        return result
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
            }
        }
        return true
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }

    fun showProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.VISIBLE
        if (tittleDividerView != null)
            tittleDividerView.visibility = View.GONE
    }

    fun hideProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.GONE

        if (tittleDividerView != null)
            tittleDividerView.visibility = View.VISIBLE
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
