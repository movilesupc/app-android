package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_create_sport_event.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.Place
import pe.com.teammoviles.yourwayapp.ui.models.SportEvent
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateSportEventActivity : AppCompatActivity() {

    private lateinit var sportEventCrear: SportEvent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_sport_event)
        backAppCompatImageButton.setOnClickListener { finish() }
        addFriendCardView.setOnClickListener { addFriendsActivity() }
        nextCardView.setOnClickListener { validateStepActivity() }

        //1 = Futbol, 2 = Atletismo, 3 = Basket
        sportTypeMaterialSpinner.setItems(
            "Futbol",
            "Atletismo",
            "Basket"
        )
    }

    private fun validateStepActivity() {
        if (validatefieldsStepActivity()) {
            if (AddFriendEventActivity.friendsSelected.size > 0) {
                choosePlaceActivity()
            }
            else {
                Toast.makeText(this, "Seleccione participantes del evento", Toast.LENGTH_SHORT).show()
            }
        }
        else {
            Toast.makeText(this, "No deje campos vacios", Toast.LENGTH_SHORT).show()
        }
    }

    private fun validatefieldsStepActivity(): Boolean {
        var isValid = true

        val name = tiName.text.toString()
        if(TextUtils.isEmpty(name)){
            isValid = false
        }

        val descripcion = tiDescription.text.toString()
        if(TextUtils.isEmpty(descripcion)){
            isValid = false
        }

        return isValid
    }

    private fun addFriendsActivity() {
        val intent = Intent(this, AddFriendEventActivity::class.java)
        startActivity(intent)
    }

    private fun choosePlaceActivity() {
        val date = getCurrentDateTime()
        //val dateInString = date.toString("yyyy/MM/dd HH:mm:ss")
        val dateInString = date.toString("yyyy-MM-dd")

        sportEventCrear = SportEvent(
            "",
            tiName.text.toString(),
            tiDescription.text.toString(),
            UserDefaults.id!!,
            (sportTypeMaterialSpinner.selectedIndex + 1).toString(),
            "2",
            "",
            "",
            "true",
            dateInString,
            Place(),
            ArrayList()
        )


        val intent = Intent(this, ChoosePlaceActivity::class.java)
        intent.putExtra("sportEventCrear", sportEventCrear as Serializable)
        startActivity(intent)
    }

    fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }
}
