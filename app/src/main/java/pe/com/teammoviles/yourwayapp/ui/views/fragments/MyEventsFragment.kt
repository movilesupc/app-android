package pe.com.teammoviles.yourwayapp.ui.views.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_my_events.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.adapters.EventAdapter
import pe.com.teammoviles.yourwayapp.ui.models.SportEvent
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyEventsFragment : Fragment() {
    private lateinit var eventAdapter: EventAdapter
    private lateinit var events: MutableList<SportEvent>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        return inflater.inflate(R.layout.fragment_my_events, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvHome.layoutManager = LinearLayoutManager(context)
    }

    override fun onResume() {
        getEvents()
        super.onResume()
    }

    private fun getEvents() {
        showProgressBar()
        IEventApi.create().getEvents(UserDefaults.id!!).enqueue(object : Callback<MutableList<SportEvent>?> {
            override fun onFailure(call: Call<MutableList<SportEvent>?>, t: Throwable) {
                hideProgressBar()
                showMessageTextView("Error al obtener los eventos")
            }

            override fun onResponse(call: Call<MutableList<SportEvent>?>, response: Response<MutableList<SportEvent>?>) {
                hideProgressBar()
                hideMessageTextView()
                if (response.isSuccessful) {
                    events = response.body()!!
                    eventAdapter = EventAdapter(events)
                    rvHome.adapter = eventAdapter
                    eventAdapter.notifyDataSetChanged()
                    if (events.count() == 0)
                        showMessageTextView("No tiene eventos creados")
                }
                else {
                    showMessageTextView("Error al obtener los eventos")
                }
            }
        })
    }

    fun showProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.VISIBLE
        if (tittleDividerView != null)
            tittleDividerView.visibility = View.GONE
    }

    fun hideProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.GONE

        if (tittleDividerView != null)
            tittleDividerView.visibility = View.VISIBLE
    }

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessageTextView(message: String) {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.VISIBLE

        if (messageTextView != null)
            messageTextView.text = message
    }

    fun hideMessageTextView() {
        if (messageConstraintLayout != null)
            messageConstraintLayout.visibility = View.GONE
    }
}
