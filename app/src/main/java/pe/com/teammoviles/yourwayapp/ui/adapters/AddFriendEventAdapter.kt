package pe.com.teammoviles.yourwayapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.prototype_add_friend_event.view.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.*

class AddFriendEventAdapter(val friends: MutableList<User>) : RecyclerView.Adapter<AddFriendEventAdapterPrototype>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddFriendEventAdapterPrototype {
        val friendInEventPrototype: AddFriendEventAdapterPrototype = AddFriendEventAdapterPrototype(
            LayoutInflater.from(parent.context).inflate(
                R.layout.prototype_add_friend_event, parent, false))
        return friendInEventPrototype
    }

    override fun getItemCount(): Int {
        return friends.size
    }

    override fun onBindViewHolder(holder: AddFriendEventAdapterPrototype, position: Int) {
        val friend = friends[position]

        holder.bind(friend)
        holder.addCardView.setOnClickListener {
            if (onItemClicked != null) {
                holder.deleteCardView.visibility = View.VISIBLE
                holder.addCardView.visibility = View.GONE

                onItemClicked!!.itemAddClicked(friend)
            }
        }

        holder.deleteCardView.setOnClickListener {
            if (onItemClicked != null) {
                holder.deleteCardView.visibility = View.GONE
                holder.addCardView.visibility = View.VISIBLE

                onItemClicked!!.itemDeleteClicked(friend)
            }
        }
    }

    interface OnItemClicked {
        fun itemAddClicked(friend: User)
        fun itemDeleteClicked(friend: User)
    }

    private var onItemClicked: OnItemClicked? = null

    fun setOnItemClicked(onItemClicked: OnItemClicked){
        this.onItemClicked = onItemClicked
    }
}

class AddFriendEventAdapterPrototype(itemView: View): RecyclerView.ViewHolder(itemView) {
    private var nameTextView = itemView.nameTextView
    private var lastNameTextView = itemView.lastNameTextView
    var addCardView = itemView.addCardView
    var deleteCardView = itemView.deleteCardView

    fun bind(user: User) {
        nameTextView.text = user.name
        lastNameTextView.text = user.lastName
        if (user.isSelected!!){
            deleteCardView.visibility = View.VISIBLE
            addCardView.visibility = View.GONE
        }
        else {
            deleteCardView.visibility = View.GONE
            addCardView.visibility = View.VISIBLE
        }
    }
}