package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        btSignup.setOnClickListener(this)

    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btSignup -> createAccount(
                tiSignupMail.text.toString(),
                tiSignupPassword.text.toString(),
                tiSignupName.text.toString(),
                tiSignupLastName.text.toString())
        }
    }

    private fun validateForm(): Boolean{
        var valid = true

        val name = tiSignupName.text.toString()
        if(TextUtils.isEmpty(name)){
            tiSignupName.error = "Required."
            valid = false
        }else{
            tiSignupName.error = null
        }

        val lastName = tiSignupLastName.text.toString()
        if(TextUtils.isEmpty(lastName)){
            tiSignupLastName.error = "Required."
            valid = false
        }else{
            tiSignupLastName.error = null
        }

        val mail = tiSignupMail.text.toString()
        if(TextUtils.isEmpty(mail)){
            tiSignupMail.error = "Required."
            valid = false
        }else{
            tiSignupMail.error = null
        }

        val password = tiSignupPassword.text.toString()
        if(TextUtils.isEmpty(password)){
            tiSignupPassword.error = "Required."
            valid = false
        }else{
            tiSignupPassword.error = null
        }

        return valid
    }

    private fun createAccount(mail: String, password: String, name: String, lastName: String){
        if(!validateForm()){
            return
        }
        Toast.makeText(this,"creando ...", Toast.LENGTH_SHORT).show()
        /*auth.createUserWithEmailAndPassword(mail, password).addOnCompleteListener(this) { task ->
            if(task.isSuccessful){
                Toast.makeText(this,R.string.toast_signup_successful, Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }else{
                Toast.makeText(this,R.string.toast_signup_faile, Toast.LENGTH_SHORT).show()
            }
        }*/

        showProgressBar()
        val json = JSONObject()
        json.put("name", name)
        json.put("lastName", lastName)
        json.put("email", mail)
        json.put("password", password)
        json.put("imageUrl", "")
        json.put("active", true)
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), json.toString())

        IEventApi.create().register(requestBody).enqueue(object : Callback<User?> {
            override fun onFailure(call: Call<User?>, t: Throwable) {
                toastMessage("Credenciales invalidas")
                hideProgressBar()
            }
            override fun onResponse(call: Call<User?>, response: Response<User?>) {
                if (response.code() == 200) {
                    UserDefaults.saveInSharedPreferences(response.body()!!, this@SignupActivity)
                    startMainActivity()
                }
                else
                    toastMessage("Error en el servidor")
                hideProgressBar()
            }
        })
    }

    private fun showProgressBar() {
        loadProgressBar.visibility = View.VISIBLE
        btSignup.visibility = View.GONE
    }

    private fun hideProgressBar() {
        loadProgressBar.visibility = View.GONE
        btSignup.visibility = View.VISIBLE
    }

    fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}




































