package pe.com.teammoviles.yourwayapp.ui.models

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class UserFriend(

    @SerializedName("userId")
    var userId: String,

    @SerializedName("friendId")
    var friendId: String,

    @SerializedName("active")
    var active: String,

    var isSelected: Boolean
) : Serializable {

}