package pe.com.teammoviles.yourwayapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.prototype_friend_in_event.view.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.*

class FriendInEventAdapter(val friends: MutableList<User>) : RecyclerView.Adapter<FriendInEventPrototype>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendInEventPrototype {
        val friendInEventPrototype: FriendInEventPrototype = FriendInEventPrototype(
            LayoutInflater.from(parent.context).inflate(
                R.layout.prototype_friend_in_event, parent, false))
        return friendInEventPrototype
    }

    override fun getItemCount(): Int {
        return friends.size
    }

    override fun onBindViewHolder(holder: FriendInEventPrototype, position: Int) {
        holder.bind(friends[position])
    }
}

class FriendInEventPrototype(itemView: View): RecyclerView.ViewHolder(itemView) {
    private var cvFriendInEvent = itemView.cvFriendInEvent
    private var friendTextView = itemView.friendTextView

    fun bind(user: User) {
        cvFriendInEvent.setOnClickListener {

        }
        friendTextView.text = user.lastName
    }
}