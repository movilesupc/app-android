package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_rate_sport_event.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.SportEvent
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateSportEventActivity : AppCompatActivity() {

    private lateinit var currentEvent: SportEvent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_sport_event)
        backAppCompatImageButton.setOnClickListener { finish() }
        intent?.extras?.apply {
            currentEvent = getSerializable("currentEvent") as SportEvent
            if (currentEvent.rating != ""){
                eventRatingBar.rating = currentEvent.rating.toFloat()
            }
        }

        rateCardView.setOnClickListener {
            rate()
        }
    }

    private fun rate() {
        showProgressBar()
        IEventApi.create().rateSportEvent(currentEvent.id, eventRatingBar.rating).enqueue(object : Callback<SportEvent?> {
            override fun onFailure(call: Call<SportEvent?>, t: Throwable) {
                showToast("Error al calificar el evento")
                hideProgressBar()
            }
            override fun onResponse(call: Call<SportEvent?>, response: Response<SportEvent?>) {
                if (response.isSuccessful) {
                    currentEvent.rating = eventRatingBar.rating.toString()
                    showToast("Se calificó el evento correctamente")
                    val i = Intent()
                    i.putExtra("rate", eventRatingBar.rating)
                    setResult(Activity.RESULT_OK, i)
                    finish()
                }
                else
                    showToast("Error al calificar el evento")
                hideProgressBar()
            }
        })
    }

    fun showProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.VISIBLE
        if (tittleDividerView != null)
            tittleDividerView.visibility = View.GONE
    }

    fun hideProgressBar() {
        if (loadProgressBar != null)
            loadProgressBar.visibility = View.GONE

        if (tittleDividerView != null)
            tittleDividerView.visibility = View.VISIBLE
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
