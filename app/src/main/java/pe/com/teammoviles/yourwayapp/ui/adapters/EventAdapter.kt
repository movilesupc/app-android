package pe.com.teammoviles.yourwayapp.ui.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.prototype_event.view.*
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.SportEvent
import pe.com.teammoviles.yourwayapp.ui.views.activities.DetailEventActivity
import java.io.Serializable

class EventAdapter(val events: List<SportEvent>): RecyclerView.Adapter<EventPrototype>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventPrototype {
        val myPrototype: EventPrototype = EventPrototype(LayoutInflater.from(parent.context)
            .inflate(R.layout.prototype_event, parent, false))
        return myPrototype
    }

    override fun getItemCount(): Int { return events.size }

    override fun onBindViewHolder(holder: EventPrototype, position: Int) {
        holder.bind(events[position])
    }
}

class EventPrototype(itemView: View): RecyclerView.ViewHolder(itemView) {
    private var tvEventTitle = itemView.tvEventTitle
    private var tvEventDescription = itemView.tvEventDescription
    private var ivEventImg = itemView.ivEventImg
    private var cvEvent = itemView.cvEvent

    fun bind(event: SportEvent) {
        tvEventTitle.text = event.title
        tvEventDescription.text = event.description
        Picasso.get()
            .load("https://blogmedia.evbstatic.com/wp-content/uploads/wpmulti/sites/31/2011/08/15180321/marathon-running-picture-id950019508.jpg")
            .into(ivEventImg)

        cvEvent.setOnClickListener {
            val i = Intent(itemView.context, DetailEventActivity::class.java)
            i.putExtra("currentEvent", event as Serializable)
            itemView.context.startActivity(i)
        }
    }
}






























