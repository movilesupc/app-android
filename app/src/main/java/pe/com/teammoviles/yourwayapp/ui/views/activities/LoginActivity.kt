package pe.com.teammoviles.yourwayapp.ui.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import pe.com.teammoviles.yourwayapp.R
import pe.com.teammoviles.yourwayapp.ui.models.User
import pe.com.teammoviles.yourwayapp.ui.models.UserDefaults
import pe.com.teammoviles.yourwayapp.ui.retrofit.IEventApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btLoginSignin.setOnClickListener(this)
        btLoginRegister.setOnClickListener(this)
        passwordVisibility()
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btLoginSignin -> signIn(tiLoginMail.text.toString(), tiLoginPassword.text.toString())
            R.id.btLoginRegister -> {
                startActivity(Intent(this, SignupActivity::class.java))
            }
        }
    }

    private fun validateForm(): Boolean{
        return tiLoginMail.text.toString().isNotEmpty() && tiLoginPassword.text.toString().isNotEmpty()
    }

    private fun signIn(email: String, password: String){
        if(!validateForm()){
            Toast.makeText(this,R.string.toast_login_signIn,Toast.LENGTH_SHORT).show()
            return
        }
        else{
            login(email, password)
        }
    }

    private fun login(email: String, password: String) {
        showProgressBar()

        val json = JSONObject()
        json.put("email", email)
        json.put("password", password)
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), json.toString())

        IEventApi.create().login(requestBody).enqueue(object : Callback<User?> {
            override fun onFailure(call: Call<User?>, t: Throwable) {
                toastMessage("Credenciales invalidas")
                hideProgressBar()
            }
            override fun onResponse(call: Call<User?>, response: Response<User?>) {
                if (response.code() == 200) {
                    UserDefaults.saveInSharedPreferences(response.body()!!, this@LoginActivity)
                    startMainActivity()
                }
                else {
                    toastMessage("Error en el servidor")
                    hideProgressBar()
                }
            }
        })
    }

    private fun showProgressBar() {
        loadProgressBar.visibility = View.VISIBLE
        btLoginSignin.visibility = View.GONE
        btLoginRegister.visibility = View.GONE
    }

    private fun hideProgressBar() {
        loadProgressBar.visibility = View.GONE
        btLoginSignin.visibility = View.VISIBLE
        btLoginRegister.visibility = View.VISIBLE
    }

    private fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun passwordVisibility() {
        tiLoginPassword.setOnFocusChangeListener { view, isFocused ->
            if (!isFocused) {
                tiLoginPasswordTextInputLayout.isPasswordVisibilityToggleEnabled = false
            }
            else {
                tiLoginPassword.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) { }
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        tiLoginPasswordTextInputLayout.isPasswordVisibilityToggleEnabled = tiLoginPassword.length() > 0
                    }
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
                })
            }
        }
    }

    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
